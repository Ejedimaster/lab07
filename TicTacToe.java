import java.util.Scanner;

public class TicTacToe
{
	public static void main(String[] args)
	{
		System.out.println("Welcome to Tic-Tac-Toe!");
		Board gameBoard = new Board();
		boolean gameOver = false;
		Scanner reader = new Scanner(System.in);
		
		int row = 0;
		int column = 0;
		
		int player = 1;
		Square playerToken = Square.X;
		
		while(gameOver == false)
		{
			System.out.print(gameBoard);
			
			if(player == 1)
			{
				playerToken = Square.X;
			}
			else
			{
				playerToken = Square.O;
			}
			
			System.out.println("Please Enter Your Token's row placement");
			row = reader.nextInt();
			System.out.println("Please Enter Your Token's column placement");
			column = reader.nextInt();
			
			boolean gameTest = gameBoard.placeToken(row, column, playerToken);
	
			while(gameTest == false)
			{
				System.out.println("Please Enter Your Token's row placement");
				row = reader.nextInt();
				System.out.println("Please Enter Your Token's column placement");
				column = reader.nextInt();
			}
			
			gameBoard.placeToken(row, column, playerToken);
			gameBoard.checkIfWinning(playerToken);
			gameBoard.checkIfFull();
			
			if(gameBoard.checkIfWinning(playerToken) == true)
			{
				System.out.print(gameBoard);
				System.out.println("Congratulations! Player " + player + " Won the Game!");
				gameOver = true;
			}
			else if(gameBoard.checkIfFull() == true)
			{
				System.out.print(gameBoard);
				System.out.println("It's a Tie! Better Luck Next Time!");
				gameOver = true;
			}
			else
			{
				player = player + 1;
				if(player > 2)
				{
					player = 1;
				}
			}
		}
	}
}