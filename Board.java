public class Board
{
	private Square[][] tictactoeBoard;
	
	public Board()
	{
		this.tictactoeBoard = new Square[3][3];
		
		for(int x = 0; x<tictactoeBoard.length; x++)
		{
			for(int y = 0; y<tictactoeBoard[x].length; y++)
			{
				this.tictactoeBoard[x][y] = Square.BLANK;
			}
		}
	}
	
	public String toString()
	{
		String boardDisplay = "";
		
		for(int i = 0; i<tictactoeBoard.length; i++)
		{
			for(int j = 0; j<tictactoeBoard[i].length; j++)
			{
				boardDisplay = boardDisplay + this.tictactoeBoard[i][j]+" ";
			}
			boardDisplay = boardDisplay + "\n";
		}
		return boardDisplay;
	}
	
	public boolean placeToken(int row, int column, Square playerToken)
	{
		boolean token = false;
		
		if(row > 3 || row < 1 || column > 3 || column < 1)
		{
			token = false;
		}
		column = column - 1;
		row = row - 1;
		
		if(this.tictactoeBoard[column][row] == Square.BLANK)
		{
			this.tictactoeBoard[column][row] = playerToken;
			token = true;
		}
		else
		{
			token = false;
		}
		return token;
	}
	
	public boolean checkIfFull()
	{
		boolean full = true;
		
		for(int m = 0; m<tictactoeBoard.length; m++)
		{
			for(int n = 0; n<tictactoeBoard[m].length; n++)
			{
				if(this.tictactoeBoard[m][n] == Square.BLANK)
				{
					full = false;
					return full;
				}
			}
		}
		return full;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken)
	{
		int trueCount = 0;
		for(int a = 0; a<tictactoeBoard.length; a++)
		{
			trueCount = 0;
			for(int b = 0; b<tictactoeBoard[a].length; b++)
			{
				if(tictactoeBoard[a][b] == playerToken)
				{
					trueCount += 1;
					if(trueCount == 3)
					{
						return true;
					}
	
				}
			}
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken)
	{
		int winV = 0;
		for(int b = 0; b<tictactoeBoard.length; b++)
		{
				winV = 0;
			for(int b2 = 0; b2<tictactoeBoard[b].length; b2++)
			{
				if(tictactoeBoard[b2][b] == playerToken)
				{
					winV += 1;
					if(winV == 3)
					{
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private boolean checkIfWinningDiagonal(Square playerToken)
	{
		int winD = 0;
		for(int a1 = 0; a1<tictactoeBoard.length; a1++)
		{
			if(tictactoeBoard[a1][a1] == playerToken)
			{
				winD += 1;
				if(winD == 3)
				{
					return true;
				}
	
			}
		}
		return false;
		
	}
	
	public boolean checkIfWinning(Square playerToken)
	{
		boolean horizontal = checkIfWinningHorizontal(playerToken);
		boolean vertical = checkIfWinningVertical(playerToken);
		boolean diagonal = checkIfWinningDiagonal(playerToken);
		
		if(vertical == true || horizontal == true ||diagonal == true)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	
}